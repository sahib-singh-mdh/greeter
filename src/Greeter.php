<?php

namespace Mdh\Greeter;

use GuzzleHttp\Client;
class Greeter
{
    private $apiKey;
    private $apiUrl;
    private $provider;

    function __construct($apiKey, $apiUrl, $provider = "active-campaign")
    {
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;
        $this->provider = $provider;
    }

    public function getList($method, $apiKey, $apiUrl, $endPoint, $body)
    {
        $client = new Client();

        switch ($method) {
            case 'GET':
                $response = $client->request($method, "$apiUrl/api/3/$endPoint", [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Api-Token' => $apiKey,
                        'Content-Type' => 'application/json',
                    ],
                ]);
                break;

            case 'POST':
                $response = $client->request($method, "$apiUrl/api/3/$endPoint", [
                    'body' => json_encode($body),
                    'headers' => [
                        'Accept' => 'application/json',
                        'Api-Token' => $apiKey,
                        'Content-Type' => 'application/json',
                    ],
                ]);
                break;

            default:
                # code...
                break;
        }

        return json_decode($response->getBody());
    }

    private function mailChimp()
    {
        return response()->json([
            'success' => true,
            'message' => 'Mailchimp Is In Development Mode',
            'response' => 'Valid Responses Are: ["active-campaign", "mail-chimp"]'
        ], 200);
    }

    private function defaultResponse()
    {
        return response()->json([
            'success' => false,
            'message' => 'Invalid Input From User',
            'response' => 'Valid Responses Are: ["active-campaign", "mail-chimp"]'
        ], 400);
    }
}
